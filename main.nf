#!/usr/bin/env nextflow


nextflow.enable.dsl = 2

params.reads = "$baseDir/data/*.fastq.gz"
params.outdir = "results"

log.info """\
 SANITY CHECK - N F   P I P E L I N E
 ===================================
 reads        : ${params.reads}
 outdir       : ${params.outdir}
 """

// import modules
include { FQTOOLS } from './modules/fqtools'

workflow {
  reads_ch = channel.fromPath( params.reads, checkIfExists: true ) 
  FQTOOLS(reads_ch)
}
 
