params.outdir = 'results'

process FQTOOLS {
	
    container = 'fqtools:latest' 

    publishDir params.outdir

    input:
    path reads

    output:
    stdout 

    shell:
    '''
    trap 'if [[ "$(fqtools validate !{reads})" == "OK" ]]; then echo -n OK; exit 0; fi' EXIT
    '''
}
