## How to run the pipeline

* Clone https://git.folkhalsomyndigheten.se/gensam/fhm-002-011.git
* Build the dockers
* Sudo command for nextflow

```bash
sudo nextflow run main.nf -profile docker
```
